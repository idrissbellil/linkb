from math import exp, pi, acos, asin, sqrt, log,cos,sin, log10

class Rain(object):
    def __init__(self,f, R, elevation=0., tilt=45.):
        h, v, a, b, c, m, cx = (0,1,0,1,2,3,4)
        
        self.k = []
        H = [[-5.33980,-0.35351,-0.23789,-0.94158],
            [-0.10008, 1.26970, 0.86036, 0.64552],
            [1.13098, 0.45400, 0.15354, 0.16817],
            [-0.18961],
            [0.71147]]
        self.k.append(H)
        V = [[-3.80595,-3.44965,-0.39902,0.50167],
            [0.56934,-0.22911,0.73042,1.07319],
            [0.81061,0.51059,0.11899,0.27195],
            [-0.16398],
            [0.63297]]
        self.k.append(V)
        
        self.alpha = []
        H = [[-0.14318,0.29591,0.32177,-5.37610,16.1721],
            [1.82442,0.77564,0.63773,-0.96230,-3.29980],
            [-0.55187,0.19822,0.13164,1.47828,3.43990],
            [0.67849],
            [-1.95537]]
        self.alpha.append(H)
        V = [[-0.07771,0.56727,-0.20238,-48.2991,48.5833],
            [2.33840,0.95545,1.14520,0.791669,0.791459],
            [-0.76284,0.54039,0.26809,0.116226,0.116479],
            [-0.053739],
            [0.83433]]
        self.alpha.append(V)
        
        self.f = f                              #(GHz)
        self.R = R                              #(mm/h)
        self.elevation = elevation/180.*pi      #entered (degrees), stocked (rad)
        self.tilt = tilt/180*pi                 #entered (degrees), stocked (rad)
        self.computeLoss()

    # write result into self.attenuation
    def computeLoss(self):
        h, v, a, b, c, m, cx = (0,1,0,1,2,3,4)
    
        bLogk, bAlpha = True, False

        logkv,logkh = self.kAlphaSum(v,bLogk),self.kAlphaSum(h,bLogk)

        alphav, alphah = self.kAlphaSum(v,bAlpha), self.kAlphaSum(h,bAlpha)

        kv, kh = (10**logkv), (10**logkh)

        dK = ( kh + kv + (kh - kv)*(cos(self.elevation)**2)*cos(2*self.tilt)) / 2.

        dAlpha = ( kh*alphah + kv*alphav + (kh*alphah - kv*alphav)*(cos(self.elevation)**2)*cos(2*self.tilt)) / (2.*dK)
         
        self.attenuation = dK*(self.R**dAlpha)      # (dB/km)

    # this avoid typing the same code 4 times
    def kAlphaSum(self, nHorV, bIsK):
        h, v, a, b, c, m, cx = (0,1,0,1,2,3,4)
        d = []
        nLength = 0
        if(bIsK):
            d = self.k
            nLength = 4
        else:
            d = self.alpha
            nLength = 5

        dSum = d[nHorV][m][0] * log10(self.f) + d[nHorV][cx][0]

        for j in range(nLength):
            dSum += d[nHorV][a][j]*exp(-((log10(self.f)-d[nHorV][b][j])/d[nHorV][c][j])**2)
        return dSum

