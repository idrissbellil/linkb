# linkb
## Description
I have used ITU-R publicly available models for gasses, rain, fog, vegetation and indoor attenuations.
Models for temperatures, water vapour, pressure and refractive Index were also used.
This program can as well estimate the EM path and tell receiving position given transmitting and receiving heights in addition to the transmitting position. More details in this [presentation](http://dspace.univ-tlemcen.dz/bitstream/112/11154/2/presentation.pdf) (it's in French)

![alt text](desktop.gif?raw=true "Launch Example")
