from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.properties import *
from kivy.clock import Clock
import re

import gases.low, gases.mid.summer, gases.mid.winter, gases.high.summer, gases.high.winter
from freespace import Freespace
from fog import Fog
from rain import Rain
from vegetation import Vegetation
from indoor import Office

# Input get filtered at this level, taken from kivy-doc
class FloatInput(TextInput):
    def __init__(self, **kwargs):
        super(FloatInput, self).__init__(multiline=False, size_hint=(2,1),**kwargs)

    pat = re.compile('[^0-9]')
    def insert_text(self, substring, from_undo = False):
        pat = self.pat
        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:
            s = '.'.join([re.sub(pat,'', s) for s in substring.split('.',1)])
        return super(FloatInput,self).insert_text(s, from_undo=from_undo) 

# made here because it will only be needed by Indoor
class System(BoxLayout):
    def __init__(self, **kwargs):
        super(System, self).__init__(**kwargs)

# Outdoor computing resides here
class Outdoor(BoxLayout):
    data = {'h1':0.,'G1':0.,'Phi':0.,'f':0.,'Pe':0.,'d':0.,'Lt':0.,'M':0.,'d1':0.,'R':0.,'d2':0.,'d3':0.,'h2':0.,'G2':0.}
    checksumOld = 0.
    checksumNew = .1
    
    def __init__(self, **kwargs):
        super(Outdoor, self).__init__(**kwargs)
        
    def computeLoss(self):
        extBool = 7*[1.] + 2*[self.bool_list[0]] + 2*[self.bool_list[1]] + [self.bool_list[2]] + 2*[1.]
        data = [float('0'+ x)*y for x, y in zip(self.text_list,extBool)]
        keys = ['h1','G1','Phi','f','Pe','d','Lt','M','d1','R','d2','d3','h2','G2']
        self.data = dict(zip(keys, data))
        self.checksumNew = sum(data)
        if abs(self.checksumOld - self.checksumNew)<0.001:
            return
        self.checksumOld = self.checksumNew
        Clock.schedule_once(self.compute, 4)
        
    def compute(self, dt):
        if abs(self.checksumOld - self.checksumNew)>0.001:
            return
        self.totalLossS = 0
        self.totalLossW = 0
        if(self.data['Lt']<=22):
            AtmosphereS = AtmosphereW = gases.low.Atmosphere
        elif(self.data['Lt']<=45):
            AtmosphereS = gases.mid.summer.Atmosphere
            AtmosphereW = gases.mid.winter.Atmosphere
        else:
            AtmosphereS = gases.high.summer.Atmosphere
            AtmosphereW = gases.high.winter.Atmosphere
            
        atmS = AtmosphereS(self.data['h1'],self.data['h2'], self.data['f'], 90 - self.data['Phi'], self.data['d'])
        self.totalLossS += atmS.gasesLoss
        atmW = AtmosphereW(self.data['h1'],self.data['h2'], self.data['f'], 90 - self.data['Phi'], self.data['d'])
        self.totalLossW += atmW.gasesLoss
        
        if abs(self.data['d']-atmS.EmD)>(1e-2) and self.data['Phi'] != 0 :
            self.d = '{0:.2f}'.format(atmS.EmD)
            self.checksumOld = self.checksumOld - self.data['d'] + atmS.EmD
            self.data['d'] = atmS.EmD
            
        if self.data['d'] == 0:
            return

        fs = Freespace(self.data['f'] * 1e9, self.data['d']*1e3)
        self.totalLossS += fs.loss
        self.totalLossW += fs.loss
        
        
        self.result += '\n' + '-'*60
        self.result += '\ngases loss:\tsummer: {0:.2f}\t winter: {1:.2f}\tdB'.format(atmS.gasesLoss,atmW.gasesLoss)
        self.result += '\nfreespace loss:\t{0:.2f} dB'.format(fs.loss)
        if self.data['Phi'] != 0 :
            self.result += '\nrelative longitude:\t{0:.4f} degree\tangle: {1:.4f}'.format((atmS.longi+atmW.longi)/2, (90-(atmS.angle+atmW.angle)/2))
        
        if self.bool_list[0]:
            temp = atmS.superLayer[0].layer.T
            fg = Fog(temp, self.data['f'], self.data['M'])
            self.totalLossS += fg.attenuation * self.data['d1']
            self.result += '\nfog loss:\tsummer: {0:.2f}'.format(fg.attenuation * self.data['d1'])
            temp = atmW.superLayer[0].layer.T
            fg = Fog(temp, self.data['f'], self.data['M'])
            self.totalLossW += fg.attenuation * self.data['d1']
            self.result += '\twinter: {0:.2f}\tdB'.format(fg.attenuation * self.data['d1'])
            
        if self.bool_list[1]:
            rn = Rain(self.data['f'], self.data['R'])
            self.totalLossS += rn.attenuation * self.data['d2']
            self.totalLossW += rn.attenuation * self.data['d2']
            self.result += '\nrain loss:\t{0:.2f}'.format(rn.attenuation * self.data['d2'])
            
        if self.bool_list[2]:
            vg = Vegetation(self.data['f']*1000, self.data['d3'])
            self.totalLossS += vg.loss
            self.totalLossW += vg.loss
            self.result += '\nVegetation loss:\t{0:.2f}'.format(vg.loss)
            
        self.result += '\ntotal loss:\tsummer: {0:.2f}\t winter: {1:.2f}\tdB'.format(self.totalLossS,self.totalLossW)
        
        PrS = self.data['Pe'] + self.data['G1'] - self.totalLossS + self.data['G2']
        PrW = self.data['Pe'] + self.data['G1'] - self.totalLossW + self.data['G2']
        
        self.result += '\nPr:\tsummer: {0:.2f}\t winter: {1:.2f}\tdB'.format(PrS,PrW)
        
        
class Indoor(System):
    data = {'Pe':0,'f':0.,'d':0.,'n':0.}
    checksumOld = 0.
    checksumNew = .1
    
    def __init__(self, **kwargs):
        super(Indoor, self).__init__(**kwargs)
        
    def computeLoss(self):
        data = [float('0'+ x) for x in self.text_list]
        keys = ['Pe','f','d','n']
        self.data = dict(zip(keys, data))
        self.checksumNew = sum(data)
        if abs(self.checksumOld - self.checksumNew)<0.001:
            return
        self.checksumOld = self.checksumNew
        Clock.schedule_once(self.compute, 4)
        
    def compute(self,d):
        off = Office(self.data['f'],self.data['d'], self.data['n'])
        self.result += '\n' + 42*'-' + '\nOffice loss: {0:.2f}\t fading: {1:.2f}\tdB'.format(off.loss,off.fading)
        self.result += '\n' + 'Pr: {0:.2f}\t +/- {1:.2f}\tdB'.format(self.data['Pe'] - off.loss,off.fading)

class RootWidget(BoxLayout):
    def __init__(self, **kwargs):
        super(RootWidget, self).__init__(**kwargs)