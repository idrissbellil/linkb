class Fog(object):
    def __init__(self, T, f, M):
        self.T = T              # K
        self.f = f              # GHz
        self.M = M              # g / m^3
        self.computeLoss()

    def computeLoss(self):
        Theta = 300.0/self.T
        f = self.f
        
        ep0 = 77.66 + 103.3*(Theta - 1)
        ep1 = 0.0671*ep0
        ep2 = 3.52

        fp =  20.20 - 146*(Theta - 1) + 316*((Theta - 1)**2)
        fs = 39.8*fp

        ep2p = f*(ep0-ep1)/(fp*(1+f*f/fp/fp)) + f*(ep1-ep2)/(fs*(1+f*f/fs/fs))
        
        ep1p = (ep0-ep1)/(1+f*f/fp/fp) + (ep1-ep2)/(1+f*f/fs/fs) + ep2
             
        eta = (2+ep1p)/ep2p
        
        Kl = 0.819*f/(ep2p*(1 + eta*eta))
        
        self.attenuation = Kl*self.M    # dB/km

