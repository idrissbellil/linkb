from math import log10, pi

class Freespace:
    def __init__(self, f, d):   # d(m), f(Hz)
        self.loss = 20.*log10(4.*pi*d*f/(3e8))
