from math import exp, pi, acos, asin, sqrt, log,cos,sin, log10

class Office(object):
    def __init__(self, f, d, floors):
        self.f = f                      # f(MHz) assumed: 0.9GHz< f < 5.8GHz
        self.d = d                      # d (m) assumed: d>1m
        self.floors = floors            # n >= 1
        self.computeN()
        self.computeLf()
        self.computeLoss()
        self.computeFading()

  # crazy interpolations are used here to get smooth results
    def computeN(self):
        f = self.f/1000.
        if(f<=1.2):
            self.N = -f/.3+36.
        elif(f<=1.3):
            self.N = 32.
        elif(f<=1.8):
            self.N = -4*f+37.2
        elif(f<=2.4):
            self.N = 30.
        else:
            self.N = -0.765*f**4+10.595*f**3-51.467*f**2+102.355*f-40.301
            
  # crazy interpolations are used here too
    def computeLf(self):
        f = self.f/1000.
        n = self.floors
        if(f<=1.8):
          self.floorPenLoss = (17./.9*f-23.) +(-15.*f+31.)*n + (2.5/.9*f-5.)*n**2
        elif(f<=2.):
          self.floorPenLoss = 11. + 4.*n
        else:
          self.floorPenLoss = (-1.1379*f**4 + 18.0258*f**3 - 101.7989*f**2 + 241.8240*f - 191.4529) + (2.1248*f**4 - 32.7701*f**3 + 180.7116*f**2 - 419.2454*f + 347.8085) * n
  
    def computeLoss(self):
        self.loss = 20.*log10(self.f)+self.N*log10(self.d)+self.floorPenLoss-28.
  
    def computeFading(self):
        f = self.f/1000.
        self.fading = 0.38111*f**3 - 2.92589*f**2 + 5.89832*f + 6.85807

