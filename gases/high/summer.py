from ..father import Layer as LF
from ..father import Atmosphere as AF
from ..father import SuperLayer as SL
from math import exp, pi, acos, asin, sqrt, log,cos,sin

class Layer(LF):
    def __init__(self, *args,**kwargs):
        super(Layer,self).__init__(*args,**kwargs)

    def computeT(self):
        if(self.altitude < 10.0):
            self.T = 286.8374 - 4.7805*self.altitude - 0.1402*self.altitude**2 # corrected from 289.8374 to 286.8374
        elif(self.altitude < 23.0):
            self.T = 225.0
        elif(self.altitude < 48.0):
            self.T = 225.*exp((self.altitude - 23.)*0.008317)
        elif(self.altitude < 53.):
            self.T = 277.
        elif(self.altitude < 79.):
            self.T = 277. - (self.altitude-53.)*4.0769
        else:
            self.T = 171.0
  
    def computeP(self):
        dP10 = 1008.0278 - 113.2494*10.0 + 3.9408*10*10
        dP72 = dP10*exp(-0.140*(72.0 - 10))
        if(self.altitude < 10.0):
            self.P = 1008.0278 - 113.2494*self.altitude + 3.9408*self.altitude**2
        elif(self.altitude < 72.0):
            self.P = dP10*exp(-0.140*(self.altitude - 10))
        else:
            self.P = dP72*exp(-0.165*(self.altitude - 72))
  
    def computeRho(self):
        if(self.altitude <= 15.0):
            self.Rho = 8.988*exp(-0.3614*self.altitude - 0.005402*self.altitude**2 - 0.001955*self.altitude**3)
        else:
            self.Rho = 0.0


class Atmosphere(AF):
    def __init__(self, *args,**kwargs):
        super(Atmosphere,self).__init__(*args,**kwargs)
        
    def computeSuperLayer(self):
        self.superLayer = []
        for iii in range(self.nLayers-1):
            self.superLayer.append(SL(Layer(self.Rn[iii]-6371.), self.f))
        
        self.superLayer.append(SL(Layer(self.h2), self.f))
  
    def directLoss(self):
        self.superLayer = []
        self.superLayer.append(SL(Layer((self.h1+self.h2)/2.0), self.f))
        self.gasesLoss = self.d*self.superLayer[0].gamma

