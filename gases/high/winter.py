from ..father import Layer as LF
from ..father import Atmosphere as AF
from ..father import SuperLayer as SL
from math import exp, pi, acos, asin, sqrt, log,cos,sin

class Layer(LF):
    def __init__(self, *args,**kwargs):
        super(Layer,self).__init__(*args,**kwargs)

    def computeT(self):
        if(self.altitude < 8.5):
            self.T = 257.4345 + 2.3474*self.altitude - 1.5479*self.altitude**2 + 0.08473*self.altitude**3
        elif(self.altitude < 30.):
            self.T = 217.5
        elif(self.altitude < 50.):
            self.T = 217.5 + (self.altitude - 30.)*2.125
        elif(self.altitude < 54.):
            self.T = 260.
        else:
            self.T = 260. - (self.altitude - 54)*1.667
  
    def computeP(self):
        dP10 = 1010.8828 - 122.2411*10 + 4.554*10*10
        dP72 = dP10*exp(-0.147*(72. - 10))
        if(self.altitude < 10.):
            self.P = 1010.8828 - 122.2411*self.altitude + 4.554*self.altitude**2
        elif(self.altitude < 72.):
          self.P = dP10*exp(-0.147*(self.altitude - 10))
        else:
          self.P = dP72*exp(-0.150*(self.altitude - 72))
  
    def computeRho(self):
        if(self.altitude <= 10):
            self.Rho = 1.2319*exp(0.07481*self.altitude - 0.0981*self.altitude**2 + 0.00281*self.altitude**3)
        else:
            self.Rho = 0.


class Atmosphere(AF):
    def __init__(self, *args,**kwargs):
        super(Atmosphere,self).__init__(*args,**kwargs)
        
    def computeSuperLayer(self):
        self.superLayer = []
        for iii in range(self.nLayers-1):
            self.superLayer.append(SL(Layer(self.Rn[iii]-6371), self.f)) 
        self.superLayer.append(SL(Layer(self.h2), self.f)) 
  
    def directLoss(self):
        self.superLayer = []
        self.superLayer.append(SL(Layer((self.h1+self.h2)/2.), self.f))
        self.gasesLoss = self.d*self.superLayer[0].gamma

