from ..father import Layer as LF
from ..father import Atmosphere as AF
from ..father import SuperLayer as SL
from math import exp, pi, acos, asin, sqrt, log,cos,sin

class Layer(LF):
    def __init__(self, *args,**kwargs):
        super(Layer,self).__init__(*args,**kwargs)

    def computeT(self):
        if(self.altitude < 10.):
            self.T = 272.7241 - 3.6217*self.altitude - 0.1759*self.altitude**2
        elif(self.altitude < 33.):
            self.T = 218.
        elif(self.altitude < 47.):
            self.T = 218. + (self.altitude - 33.)*3.5571
        elif(self.altitude < 53.):
            self.T = 255.
        elif(self.altitude < 80.):
            self.T = 255. - (self.altitude-53.)*2.0370
        else:
            self.T = 210.
  
    def computeP(self):
        dP10 = 1018.8627 - 124.2954*10 + 4.8307*10*10
        dP72 = dP10*exp(-0.147*(72. - 10.))
        if(self.altitude < 10.):
            self.P = 1018.8627 - 124.2954*self.altitude + 4.8307*self.altitude**2
        elif(self.altitude < 72.):
            self.P = dP10*exp(-0.147*(self.altitude - 10.))
        else:
            self.P = dP72*exp(-0.155*(self.altitude - 72.))
  
    def computeRho(self):
        if(self.altitude <= 10.):
          self.Rho = 3.4742*exp(-0.2697*self.altitude - 0.03604*self.altitude**2 + 0.0004489*self.altitude**3)
        else:
          self.Rho = 0.


class Atmosphere(AF):
    def __init__(self, *args,**kwargs):
        super(Atmosphere,self).__init__(*args,**kwargs)
        
    def computeSuperLayer(self):
        self.superLayer = []
        for iii in range(self.nLayers-1):
            self.superLayer.append(SL(Layer(self.Rn[iii]-6371.), self.f))

        self.superLayer.append(SL(Layer(self.h2), self.f))
  
    def directLoss(self):
        self.superLayer = []
        self.superLayer.append(SL(Layer((self.h1+self.h2)/2.), self.f))
        self.gasesLoss = self.d*self.superLayer[0].gamma

