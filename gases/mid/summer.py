from ..father import Layer as LF
from ..father import Atmosphere as AF
from ..father import SuperLayer as SL
from math import exp, pi, acos, asin, sqrt, log,cos,sin

class Layer(LF):
    def __init__(self, *args,**kwargs):
        super(Layer,self).__init__(*args,**kwargs)

    def computeT(self):
        if(self.altitude < 13.):
            self.T = 294.9838 - 5.2159*self.altitude - 0.07109*self.altitude**2
        elif(self.altitude < 17.):
            self.T = 215.15
        elif(self.altitude < 47.):
            self.T = 215.15*exp((self.altitude - 17.)*0.008128)
        elif(self.altitude < 53.):
            self.T = 275.
        elif(self.altitude < 80.):
            self.T = 275. + (1.-exp((self.altitude-53.)*0.06))*20.
        else:
            self.T = 175.
  
    def computeP(self):
        dP10 = 1012.8186 - 111.5569*10 + 3.8646*10*10
        dP72 = dP10*exp(-0.147*(72. - 10))
        if(self.altitude < 10.):
            self.P = 1012.8186 - 111.5569*self.altitude + 3.8646*self.altitude**2
        elif(self.altitude < 72.):
            self.P = dP10*exp(-0.147*(self.altitude - 10.))
        else:
            self.P = dP72*exp(-0.165*(self.altitude - 72.))
  
    def computeRho(self):
        if(self.altitude <= 15.):
            self.Rho = 14.3542*exp(-0.4174*self.altitude - 0.02290*self.altitude**2 + 0.001007*self.altitude**3)
        else:
            self.Rho = 0.


class Atmosphere(AF):
    def __init__(self, *args,**kwargs):
        super(Atmosphere,self).__init__(*args,**kwargs)
        
    def computeSuperLayer(self):
        self.superLayer = []
        for iii in range(self.nLayers-1):
            self.superLayer.append(SL(Layer(self.Rn[iii]-6371.), self.f)) 

        self.superLayer.append(SL(Layer(self.h2), self.f)) 
  
    def directLoss(self):
        self.superLayer = []
        self.superLayer.append(SL(Layer((self.h1+self.h2)/2.), self.f))
        self.gasesLoss = self.d*self.superLayer[0].gamma

