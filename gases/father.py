from math import exp, pi,cos,sin, acos, asin, sqrt, log

class Layer(object):
    def __init__(self, altitude):
        self.altitude = altitude
        self.computeT()
        self.computeP()
        self.computeRho()
        self.computeN()
        
# All these methods must be overloaded
    def computeT(self):
        self.T = 0.0
        
    def computeP(self):
        self.P = 0.0
    
    def computeRho(self):
        self.Rho = 0.0
        
    def computeN(self):
        e = self.Rho * self.T / 216.7
        N = 77.6 / self.T * (self.P + 4810.0 * e / self.T)
        self.n = 1.0 + N * 1e-6


class SuperLayer(object):
    def __init__(self, layer, f):
        self.layer = layer
        self.f = f
        self.computeGamma()

    def computeGamma(self):
        Theta = 300.0 / self.layer.T
        e = self.layer.Rho * self.layer.T / 216.7
        
#        Dry air pressure
        p = self.layer.P - e
        
        d = 5.6e-4 * (p + e) * Theta ** 0.8

        ND = self.f * p * Theta**2 * (6.14e-5/d/(1.0+(self.f/d)**2)\
             + 1.4e-12 * p * Theta**1.5 / (1.+1.9e-5 * self.f**1.5))
    
        a =[[50.474214,50.987745,51.50336,52.021429,52.542418,53.066934,53.595775,54.130025,54.67118,55.221384,55.783815,56.264774,56.363399,56.968211,57.612486,58.323877,58.446588,59.164204,59.590983,60.306056,60.434778,61.150562,61.800158,62.41122,62.486253,62.997984,63.568526,64.127775,64.67891,65.224078,65.764779,66.302096,66.836834,67.369601,67.900868,68.431006,68.960312,118.750334,368.498246,424.76302,487.249273,715.392902,773.83949,834.145546],
[0.975,2.529,6.193,14.32,31.24,64.29,124.6,227.3,389.7,627.1,945.3,543.4,1331.8,1746.6,2120.1,2363.7,1442.1,2379.9,2090.7,2103.4,2438.0,2479.5,2275.9,1915.4,1503.0,1490.2,1078.0,728.7,461.3,274.0,153.0,80.4,39.8,18.56,8.172,3.397,1.334,940.3,67.4,637.7,237.4,98.1,572.3,183.1],
[9.651,8.653,7.709,6.819,5.983,5.201,4.474,3.8,3.182,2.618,2.109,0.014,1.654,1.255,0.91,0.621,0.083,0.387,0.207,0.207,0.386,0.621,0.91,1.255,0.083,1.654,2.108,2.617,3.181,3.8,4.473,5.2,5.982,6.818,7.708,8.652,9.65,0.01,0.048,0.044,0.049,0.145,0.141,0.145],
[6.69,7.17,7.64,8.11,8.58,9.06,9.55,9.96,10.37,10.89,11.34,17.03,11.89,12.23,12.62,12.95,14.91,13.53,14.08,14.15,13.39,12.92,12.63,12.17,15.13,11.74,11.34,10.88,10.38,9.96,9.55,9.06,8.58,8.11,7.64,7.17,6.69,16.64,16.4,16.4,16.0,16.0,16.2,14.7],
[0.0,0.0,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0,0.,0,0.,0,0.,0,0.,0,0.,0,0.,0,0.,0,0.,0,0.,0,0],
[2.566,2.246,1.947,1.667,1.388,1.349,2.227,3.17,3.558,2.56,-1.172 ,3.525,-2.378 ,-3.545 ,-5.416 ,-1.932 ,6.768,-6.561 ,6.957,-6.395 ,6.342,1.014,5.014,3.029,-4.499 ,1.856,0.658,-3.036 ,-3.968 ,-3.528 ,-2.548 ,-1.660 ,-1.680 ,-1.956 ,-2.216 ,-2.492 ,-2.773 ,-0.439 ,0.,0,0.,0,0.,0],
[6.85,6.8,6.729,6.64,6.526,6.206,5.085,3.75,2.654,2.952,6.135,-0.978 ,6.547,6.451,6.056,0.436,-1.273 ,2.309,-0.776 ,0.699,-2.825 ,-0.584 ,-6.619 ,-6.759 ,0.844,-6.675 ,-6.139 ,-2.895 ,-2.590 ,-3.680 ,-5.002 ,-6.091 ,-6.393 ,-6.475 ,-6.545 ,-6.600 ,-6.650 ,0.079,0.,0,0.,0,0.,0]]
        b =[[22.23508,67.80396,119.99594,183.310091,321.225644,325.152919,336.222601,380.197372,390.134508,437.346667,439.150812,443.018295,448.001075,470.888947,474.689127,488.491133,503.568532,504.482692,547.67644,552.02096,556.936002,620.700807,645.866155,658.00528,752.033227,841.053973,859.962313,899.306675,902.616173,906.207325,916.171582,923.118427,970.315022,987.926764,1780.000000],
[0.113,0.0012,0.0008,2.42,0.0483,1.499,0.0011,11.52,0.0046,0.065,0.9218,0.1976,10.32,0.3297,1.262,0.252,0.039,0.013,9.701,14.77,487.4,5.012,0.0713,0.3022,239.6,0.014,0.1472,0.0605,0.0426,0.1876,8.34,0.0869,8.972,132.1,22300.0000],
[2.143,8.735,8.356,0.668,6.181,1.54,9.829,1.048,7.35,5.05,3.596,5.05,1.405,3.599,2.381,2.853,6.733,6.733,0.114,0.114,0.159,2.2,8.58,7.82,0.396,8.18,7.989,7.917,8.432,5.111,1.442,10.22,1.92,0.258,0.952],
[28.11,28.58,29.48,30.5,23.03,27.83,26.93,28.73,21.52,18.45,21.0,18.6,26.32,21.52,23.55,26.02,16.12,16.12,26.0,26.0,32.1,24.38,18.0,32.1,30.6,15.9,30.6,29.85,28.65,24.08,26.7,29.0,25.5,29.85,176.2],
[0.69,0.69,0.7,0.64,0.67,0.68,0.69,0.54,0.63,0.6,0.63,0.6,0.66,0.66,0.65,0.69,0.61,0.61,0.7,0.7,0.69,0.71,0.6,0.69,0.68,0.33,0.68,0.68,0.7,0.7,0.7,0.7,0.64,0.68,0.5],
[4.8,4.93,4.78,5.3,4.69,4.85,4.74,5.38,4.81,4.23,4.29,4.23,4.84,4.57,4.65,5.04,3.98,4.01,4.5,4.5,4.11,4.68,4.0,4.14,4.09,5.76,4.09,4.53,5.1,4.7,4.78,5.0,4.94,4.55,30.5],
[1.0,0.82,0.79,0.85,0.54,0.74,0.61,0.89,0.55,0.48,0.52,0.5,0.67,0.65,0.64,0.72,0.43,0.45,1.0,1.0,1.0,0.68,0.5,1.0,0.84,0.45,0.84,0.9,0.95,0.53,0.78,0.8,0.67,0.9,5.0]]

#      resolving everything related to O2
#      as stated by ITU-R P.676-10
        FiO = []
        SiO = []
        dSumO = 0.0

        SmallDeltaO = []
        DeltaFO = []
      
        for iii in range(44):
            SmallDeltaO.append((a[5][iii] + a[6][iii]*Theta)*1e-4 * (p+e)*Theta**0.8)
            DeltaFO.append(a[3][iii]*1e-4*(p*Theta**(0.8-a[4][iii])+1.1*e*Theta))
            DeltaFO[iii] = (DeltaFO[iii]**2 +2.25e-6)**.5
            FiO.append(self.f/a[0][iii]*((DeltaFO[iii]-SmallDeltaO[iii]*(a[0][iii]-self.f))/((a[0][iii]-self.f)**2+(DeltaFO[iii])**2) + (DeltaFO[iii]-SmallDeltaO[iii]*(a[0][iii]+self.f))/((a[0][iii]+self.f)**2+(DeltaFO[iii])**2)))
            SiO.append(a[1][iii]*1e-7*p* Theta**3*exp(a[2][iii]*(1-Theta)))
            dSumO += FiO[iii]*SiO[iii]
     
# resolving everything related to water vapour
# as stated by ITU-R P.676-10
        FiW = []
        SiW = []
        dSumW = 0.0

        SmallDeltaW = [0.0 for u in range(35)]
        DeltaFW = []
      
        for iii in range(35):
            DeltaFW.append(b[3][iii]*1e-4*(p*Theta**b[4][iii]+b[5][iii]*e*Theta**b[6][iii]))
            DeltaFW[iii] = 0.535*DeltaFW[iii] + (0.217*DeltaFW[iii]**2+2.1316e-12*b[0][iii]*b[0][iii]/Theta)**.5
            FiW.append(self.f/b[0][iii]*((DeltaFW[iii]-SmallDeltaW[iii]*(b[0][iii]-self.f))/((b[0][iii]-self.f)**2+DeltaFW[iii]**2) + (DeltaFW[iii]-SmallDeltaW[iii]*(b[0][iii]+self.f))/((b[0][iii]+self.f)**2+DeltaFW[iii]**2)))
            SiW.append(b[1][iii]*1e-1*e*Theta**3.5*exp(b[2][iii]*(1-Theta)))
            dSumW += FiW[iii]*SiW[iii]

        N = dSumO + dSumW + ND
        self.gamma = 0.1820*self.f*N


class Atmosphere(object):
    def __init__(self, h1, h2, f, Beta, d):
        self.h1 = min([h1,h2])
        
#       atmosphere ends at 100km, if > force it to 100km
        self.h2 = min([max([h1,h2]), 100])

        self.f = f
        self.Beta = Beta / 180.0 * pi
        self.d = d
        
#       EM distance = d if not slant
        self.EmD = 0
        self.computeStep()
        self.computeRn()
#       computeSuperLayer()
        self.computeLoss()

    def computeRn(self):
        self.Rn = []
        self.Rn.append(6371 + self.h1)
        for iii in range(1,self.nLayers):
            self.Rn.append( self.Rn[iii-1] + self.smallDelta[iii])

    def computeSuperLayer(self):
        pass
  
  
    def computeLoss(self):
        if(self.Beta < (89.0 /180.0*pi)):
            self.slantLoss()
        else:
            self.directLoss()

#   MUST BE OVERLOADED (-_-)
    def directLoss(self):
        pass
        
        
    def slantLoss(self):
        self.computeSuperLayer()
        a = []
        Alpha = []
        Beta = []

    # 1st iteration since Beta[1] must be input Beta member element
        Beta.append(self.Beta)
        a.append(-self.Rn[0]*cos(Beta[0])+.5*sqrt(4*self.Rn[0]**2*cos(Beta[0])**2+ 8*self.Rn[0]*self.smallDelta[0]+4*self.smallDelta[0]**2))
        
        Alpha.append( pi - acos((-a[0]**2-2*self.Rn[0]*self.smallDelta[0]-self.smallDelta[0]**2)/(2*a[0]*self.Rn[0]+2*a[0]*self.smallDelta[0])))
    
        dLoss = a[0]*self.superLayer[0].gamma
    
        self.EmD += a[0]
        self.longitude = []
        self.longitude.append(asin(a[0]*sin(Beta[0])/(self.Rn[0]+self.smallDelta[0])))
        gi = []
        gi.append((Beta[0])*self.smallDelta[0])
    # the same thing apply if we change the order
        for iii in range( 1, self.nLayers ):
            Beta.append( asin(self.superLayer[iii-1].layer.n/self.superLayer[iii].layer.n*sin(Alpha[iii-1])))
            a.append(-self.Rn[iii]*cos(Beta[iii])+ .5*sqrt(4*(self.Rn[iii]*cos(Beta[iii]))**2+ 8*self.Rn[iii]*self.smallDelta[iii]+4*self.smallDelta[iii]**2))
            Alpha.append(pi - acos((-a[iii]**2-2*self.Rn[iii]*self.smallDelta[iii]-self.smallDelta[iii]**2)/(2*a[iii]*self.Rn[iii]+2*a[iii]*self.smallDelta[iii])))
            self.longitude.append(asin(a[iii]*sin(Beta[iii])/(self.Rn[iii]+self.smallDelta[iii])))

            # final Loss hot and ready :o
            dLoss += a[iii]*self.superLayer[iii].gamma
            self.EmD += a[iii]
            gi.append((Beta[iii])*self.smallDelta[iii])
    
        self.gasesLoss = dLoss # I hope this will be the last
        self.longi = sum(self.longitude)/pi*180

        self.angle = sum(gi)/sum(self.smallDelta)/pi*180. + self.longi

  # steps are considered constant so atmosphere is graduated (virualy)
  # just to avoid processing of useless data in case of ressource limited devices
    def computeStep(self):
        dN1 = self.indexAt(self.h1)
        nN1 = int(dN1)  # trunk to the step just before if not a step

        dN2 = self.indexAt(self.h2)
        nN2 = int(dN2)  # trunk to the step just before if not a step

        # substracte one if higher bound mushes exactly a step
        if( float(nN2)==dN2 ):
            nN2 = nN2 - 1

        # adding first and last values
        length = nN2 - nN1 + 2   

        # let s allocate memory for our steps
        self.smallDelta = []

        # 1st can be a step or not
        if (float(nN1)==dN1):
            self.smallDelta.append(self.stepAt(nN1))
        else:
            self.smallDelta.append((self.altitudeAt(nN1+1)-self.h1))

        # this cannot change
        for iii in range(1,length-1):
            self.smallDelta.append(self.stepAt(nN1+iii))

        # last can be a step or not
        if (float(nN2)==dN2):
            self.smallDelta.append(self.stepAt(nN2))
        else:
            self.smallDelta.append( self.h2 - self.altitudeAt(nN2))
        
        self.nLayers = length
        
  #********************************************************
  # compute Index of the virtual array of steps
  # from altitude
    def indexAt(self, h):
        dN = 100.0*log(1.0 - h*exp(1.0/100)*1e4*(1.0 - exp(1.0/100))) - 1.0
        return dN

  # compute altitude at a given index
    def altitudeAt(self, nN):
        U0 = 0.0001*exp(-1.0/100)
        q = exp(1.0/100)
        h = U0*(1.0-q**(nN+1))/(1.0-q)
        return h
  
  # compute step at a given index
    def stepAt(self, i):
        delta = 1e-4*exp((i-1)/100.0)
        return delta

