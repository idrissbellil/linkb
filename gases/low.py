from .father import Layer as LF
from .father import Atmosphere as AF
from .father import SuperLayer as SL
from math import exp, pi, acos, asin, sqrt, log,cos,sin

class Layer(LF):
    def __init__(self, *args,**kwargs):
        super(Layer,self).__init__(*args,**kwargs)

    def computeT(self):
        if(self.altitude < 17.):
            self.T = 300.4222 - 6.3533*self.altitude + 0.005886*self.altitude**2
        elif(self.altitude < 47.):
            self.T = 194. + (self.altitude - 17.)*2.533
        elif(self.altitude < 52.):
            self.T = 270.
        elif(self.altitude < 80.):
            self.T = 270. - (self.altitude - 52.)*3.0714
        else:
            self.T = 184.
  
    def computeP(self):
        dP10 = 1012.0306 - 109.0338*10 + 3.6316*10*10
        dP72 = dP10*exp(-0.147*(72. - 10.))
        if(self.altitude < 10.):
          self.P = 1012.0306 - 109.0338*self.altitude + 3.6316*self.altitude**2
        elif(self.altitude < 72.):
          self.P = dP10*exp(-0.147*(self.altitude - 10.))
        else:
          self.P = dP72*exp(-0.165*(self.altitude - 72.))
  
    def computeRho(self):
        if(self.altitude <= 15.):
          self.Rho = 19.6542*exp(-0.2313*self.altitude - 0.1122*self.altitude**2 + 0.01351*self.altitude**3 - 0.0005923*self.altitude**4.)
        else:
          self.Rho = 0.


class Atmosphere(AF):
    def __init__(self, *args,**kwargs):
        super(Atmosphere,self).__init__(*args,**kwargs)
        
    def computeSuperLayer(self):
        self.superLayer = []
        for iii in range(self.nLayers-1):
            self.superLayer.append(SL(Layer(self.Rn[iii]-6371.), self.f))

        self.superLayer.append(SL(Layer(self.h2), self.f))
  
    def directLoss(self):
        self.superLayer = []
        self.superLayer.append(SL(Layer((self.h1+self.h2)/2.), self.f))
        self.gasesLoss = self.d*self.superLayer[0].gamma

