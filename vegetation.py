class Vegetation(object):
    def __init__(self, f, d, theta=0.):
        A, B, C, E, G = 0.25, 0.39, 0.25, 1.9418e-32, 0.05
        self.loss = A * f**B * d**C * (theta + E)**G
# f(MHz), d(m), theta(degrees)