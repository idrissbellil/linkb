from kivy.app import App
from rootWidgt import RootWidget

class LinkbApp(App):
    def build(self):
        return RootWidget()
        
if __name__=='__main__':
    LinkbApp().run()
